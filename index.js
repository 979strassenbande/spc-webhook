const Discord = require("discord.js");
const CoinGecko = require("coingecko-api");
const config = require("./config.json");
const webhookClient = new Discord.WebhookClient(config.webhookID, config.webhookToken);
const CoinGeckoClient = new CoinGecko();

const waitTime = config.interval * 60 * 500; // increase for bigger delay 

const sendPrices = async () => {

  let data_new = await CoinGeckoClient.coins.fetch('spaceport-coin', {
    tickers: true,
    market_data: true,

  });

  var coin = data_new.data.name;
  var market_sentiment_up = data_new.data.sentiment_votes_up_percentage;
  var market_sentiment_down = data_new.data.sentiment_votes_down_percentage
  var price = data_new.data.market_data.current_price.usd.toFixed(12) + "$";
  var mcap = data_new.data.market_data.market_cap.usd;
  var change24h = data_new.data.market_data.price_change_percentage_24h;
  var volume = data_new.data.market_data.total_volume.usd;

  
  obj = {"up" : market_sentiment_up, "down" : market_sentiment_down}
  decide = (obj.up > obj.down) ? obj.up : obj.down;
  var emb = {
    title: "Info",
    fields: []
  }
  emb.fields.push({name: coin, value: "https://spaceport.to/"}, {name: "Market sentiment", value: decide + "% UP"}, {name: "Current price:", value: price}, {name: "24h change:", value: change24h+"%"}, {name: "Volume:", value: volume+"$"});

  webhookClient.send({embeds: [emb]})
  
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;
  console.log("Webhook sent at " + dateTime);
}

const delay = async () => {
  return new Promise(resolve => setTimeout(resolve, waitTime));
}

(async () => {
  while (true) {
    await sendPrices();
    await delay();
  }
})();
